package com.example.victor.fractionapp;

/*
 * Created by victor on 27.10.16.
 */
class Fraction {
    private int nominator; //Числитель
    private int denominator; //Знаменатель

    public Fraction () {
        nominator = 0;
        denominator = 1;
    }
    public Fraction (Fraction f) {
        nominator = f.nominator;
        denominator = f.denominator;
    }

    public Fraction (int n, int d) {
        nominator = n;
        denominator = d;
    }

    public Fraction add (Fraction f) {
        return new Fraction(this.nominator + f.nominator , this.denominator);
    }
    public Fraction sub(Fraction f2) {
        return new Fraction(this.nominator - f2.nominator , this.denominator);
    }

    public Fraction mult(Fraction f2) {
        return new Fraction(this.nominator * f2.nominator , this.denominator * f2.denominator);
    }

    public Fraction div(Fraction f2) {
        return new Fraction(this.nominator / f2.nominator , this.denominator / f2.denominator); //go commit
    }
    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

}
